package siw.validator;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import siw.model.Attivita;

@Component
public class AttivitaValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Attivita.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
        
		Attivita a = (Attivita)target;
		//DataNascita
		if(a.getData()==null) 
		{		
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "data", "required");
		}
		else
		{
			if(new Date().after(a.getData())) 
			{
				errors.rejectValue("data", "noFuture");
			}
		}
	}

}
