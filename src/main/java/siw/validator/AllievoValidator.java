package siw.validator;


import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import siw.model.Allievo;

@Component
public class AllievoValidator implements Validator {
	
	private static final String EMAILREGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@Override
	public boolean supports(Class<?> clazz) {
		return Allievo.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		//Nome
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		//Cognome
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
		//Luogo Nascita
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "luogoNascita", "required");
		
		Allievo a = (Allievo)target;
		
		
		//Email
		if(a.getEmail().replace(" ","").equals(""))
		{
			errors.rejectValue("email", "required");
		}
		else
		{
			if(!a.getEmail().matches(EMAILREGEX)) 
			{
				errors.rejectValue("email", "notMatch");
			}
		}

		//Telefono
		if(a.getTelefono().replace(" ","").equals(""))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
		}
		else 
		{
			try	{ 
				Long tel = Long.parseLong(a.getTelefono());
				if(tel < 0){
					errors.rejectValue("telefono", "negative");
				}
				else {
					if(a.getTelefono().length()<10) {
						errors.rejectValue("telefono", "tooShort");
					}
				}
			}
			catch(NumberFormatException e) {
			     errors.rejectValue("telefono", "isNumber");
			}
		}
				
		//DataNascita
		if(a.getDataNascita()==null) 
		{		
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dataNascita", "required");
		}
		else
		{
			if(new Date().before(a.getDataNascita())) 
			{
				errors.rejectValue("dataNascita", "notPastt");
			}
		}
			
	}

}
