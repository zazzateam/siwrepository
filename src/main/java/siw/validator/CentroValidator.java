package siw.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import siw.model.Centro;


@Component
public class CentroValidator implements Validator{

	private static final String EMAILREGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			                                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Centro.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		//Nome
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		//Indirizzo
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "indirizzo", "required");
		//Indirizzo
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "citta", "required");
		
		Centro c= (Centro) target;
		
		
		//Email
		if(c.getEmail().replace(" ","").equals(""))
		{
			errors.rejectValue("email", "required");
		}
		else
		{
			if(!c.getEmail().matches(EMAILREGEX)) 
			{
				errors.rejectValue("email", "notMatch");
			}
		}
		
		//CapienzaMax
		if(c.getCapienzaMax().replace(" ","").equals(""))
		{
			errors.rejectValue("capienzaMax", "required");
		}
		else 
		{		
			try	{ 
				int cap = Integer.parseInt(c.getCapienzaMax());
				if(cap <=0){
					errors.rejectValue("capienzaMax", "negative");
				}
			}
			catch(NumberFormatException e) {
			     errors.rejectValue("capienzaMax", "isNumber");
			}
		}
		
		
		//Telefono
		if(c.getTelefono().replace(" ","").equals(""))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
		}
		else 
		{
			try	{ 
				Long tel = Long.parseLong(c.getTelefono());
				if(tel < 0){
					errors.rejectValue("telefono", "negative");
				}
				else {
					if(c.getTelefono().length()<10) {
						errors.rejectValue("telefono", "tooShort");
					}
				}
			}
			catch(NumberFormatException e) {
			     errors.rejectValue("telefono", "isNumber");
			}
		}
		
	}

}
