package siw.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import siw.model.Responsabile;

@Component
public class ResponsabileValidator implements Validator{
	
	private static final String EMAILREGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Responsabile.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Responsabile r = (Responsabile)target;
		
		//Email
		if(r.getEmail().replace(" ","").equals(""))
		{
			errors.rejectValue("email", "required");
		}
		else
		{
			if(!r.getEmail().matches(EMAILREGEX)) 
			{
				errors.rejectValue("email", "notMatch");
			}
		}
		
		//Password
		if(r.getPassword().replace(" ","").equals(""))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required");
		}
		else
		{
			if(r.getPassword().length()<8) {
				errors.rejectValue("password", "tooShort");
			}
		}
		
		//Telefono
		if(r.getTelefono().replace(" ","").equals(""))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
		}
		else 
		{
			try	{ 
				Long tel = Long.parseLong(r.getTelefono());
				if(tel < 0){
					errors.rejectValue("telefono", "negative");
				}
				else {
					if(r.getTelefono().length()<10) {
						errors.rejectValue("telefono", "tooShort");
					}
				}
			}
			catch(NumberFormatException e) {
			     errors.rejectValue("telefono", "isNumber");
			}
		}
	}

}
