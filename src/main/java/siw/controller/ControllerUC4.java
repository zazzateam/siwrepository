package siw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import siw.model.Attivita;
import siw.model.Centro;
import siw.service.AllievoService;
import siw.service.AttivitaService;
import siw.service.ResponsabileService;

@Controller
public class ControllerUC4 {
	
	@Autowired
	private AllievoService allievoService;
	@Autowired
	private AttivitaService attivitaService;
	@Autowired
	private ResponsabileService responsabileService;
	
	//CASO D'USO 4 : MOSTRA ATTIVITA A CUI E' ISCRITTO UN ALLIEVO
	@RequestMapping("/reportAllievo") //Da adminPanel -> selezioneAllievo
	public String reportAllievo(Model model,Authentication auth)
	{
		//Faccio il centro , e mostro solo utenti che abbiano avuto a che fare con almeno un attivita
		Centro c = responsabileService.findByEmail(auth.getName()).get(0).getCentro();
		model.addAttribute("allievi", allievoService.AllieviDiUnCentro(c.getId()));
		return "admin/scegliAllievo";
	}
	
	@RequestMapping("/elencoAttività") //Da selezioneAllievo -> mostraAttivita
	public String elencoAttivita(@RequestParam("email") String email, Model model,Authentication auth)
	{
		List<Attivita> prendiAttivita = attivitaService.prendiAttivita(email);
		System.out.println(prendiAttivita.size());
		model.addAttribute("attivita", prendiAttivita);
		return "admin/elencoAttivita";
	}

}
