package siw.controller;


import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import siw.model.Centro;
import siw.model.Responsabile;
import siw.service.CentroService;
import siw.service.ResponsabileService;
import siw.validator.CentroValidator;
import siw.validator.ResponsabileValidator;


@Controller
public class ControllerUC1 {

	@Autowired
	private ResponsabileService responsabileService;
	@Autowired
	private CentroService centroService;
	@Autowired
	private CentroValidator centroValidator;
	@Autowired
	private ResponsabileValidator responsabileValidator;

	//CASO D'USO 1 : INSERIMENTO NUOVO CENTRO (NUOVO RESPONSABILE , OGNI RESP HA UN SOLO CENTRO)
	
	//Da superAdminPanel -> pagina inserimento centro
	@RequestMapping("/addCentro")
	public String addCentro(Model model)
	{
		model.addAttribute("centro", new Centro());
		return "superAdmin/addCentro";
	}

	//Da pagina inserimento centro -> pagina inserimento responsabile
	@RequestMapping("/addResponsabile")
	public String addResponsabile(@Valid @ModelAttribute("centro") Centro centro, BindingResult br, Model model, HttpSession session)
	{
		if(this.centroService.findByEmail(centro.getEmail().toLowerCase()) != null)
		{
			model.addAttribute("exist","Esiste già un centro avente email : '"+centro.getEmail()+"'");
			return "superAdmin/addCentro";
		}
		if(this.centroService.findByIndirizzoAndCitta(centro.getIndirizzo().toLowerCase(),centro.getCitta().toLowerCase()) != null)
		{
			model.addAttribute("exist","Esiste già un centro all'indirizzo : '"+centro.getIndirizzo()+","+centro.getCitta()+"'");
			return "superAdmin/addCentro";
		}
		centroValidator.validate(centro, br);
		if(!br.hasErrors()) {
			session.setAttribute("centro", centro);
			model.addAttribute("responsabile", new Responsabile());
			return "superAdmin/addResponsabile";
		}
		else return "superAdmin/addCentro";
	}

	@RequestMapping("/registraCentro") 
	public String registroCentro(@Valid @ModelAttribute("responsabile") Responsabile responsabile, BindingResult br, Model model, HttpSession session)
	{
		if(this.responsabileService.findByEmail(responsabile.getEmail()).size()>0)
		{
			model.addAttribute("exist","Esiste già un responsabile con questa mail");
			return "superAdmin/addResponsabile";
		}
		else
		{
			responsabileValidator.validate(responsabile, br);
	
			if(!br.hasErrors()) {
				Centro centro = (Centro)session.getAttribute("centro");
				centro.setIndirizzo(centro.getIndirizzo().toLowerCase());
				centro.setCitta(centro.getCitta().toLowerCase());
				centro.setEmail(centro.getEmail().toLowerCase());
				putIntoDatabase(centro,responsabile);
				return "superAdmin/superAdminPanel";
			}
			else return "superAdmin/addResponsabile";
		}
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
	    return new BCryptPasswordEncoder();
	}
	
	public void putIntoDatabase(Centro c,Responsabile r)
	{
		BCryptPasswordEncoder b = bCryptPasswordEncoder();
		//1 : salvo centro senza responsabile (poichè r ancora non esiste nel db)
		centroService.save(c);
		//2 : salvo il responsabile dopo aver settato il centro
		c.setResponsabile(r);
		r.setCentro(c);
		r.setRuolo("RESPONSABILE");
		r.setPassword(b.encode(r.getPassword()));
		//3 : setto il responsabile (che ora esiste nel db) e aggiorno il centro
		responsabileService.save(r);
		centroService.save(c);
	}
}
