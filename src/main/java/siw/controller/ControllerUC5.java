package siw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import siw.model.Centro;
import siw.service.AllievoService;
import siw.service.AttivitaService;
import siw.service.ResponsabileService;

@Controller
public class ControllerUC5 {

	@Autowired
	private AttivitaService attivitaService;
	@Autowired
	private AllievoService allievoService;
	@Autowired
	private ResponsabileService responsabileService;
	
	//CASO D'USO 5 : MOSTRA ALLIEVI PARTECIPANTI A UN ATTIVITA
	@RequestMapping("/formReportAttività") //AdminPanel -> FormAttivita
	public String reportAttivita(Model model,Authentication auth){
		
		//Faccio il centro , e mostro solo attività offerta dal centro
		Centro centro = this.responsabileService.findByEmail(auth.getName()).get(0).getCentro();
		model.addAttribute("listaAttivita",attivitaService.findByCentro_id(centro.getId()));
		return "admin/formReportAttivita";
	}
	
	@RequestMapping("/mostraReportAttivita") //FormAttivita -> reportAttivita
	public String mostraReportAttivita(@RequestParam("id_attivita") String idAtt , Model model,Authentication auth)
	{
		model.addAttribute("listaAllievi",allievoService.prendiAllievi(idAtt));
		return "admin/reportAttivita";
	}
}
