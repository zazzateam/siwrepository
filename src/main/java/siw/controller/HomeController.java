package siw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import siw.service.CentroService;

@Controller
public class HomeController {
	
	@Autowired
	private CentroService centroService;
	
	@RequestMapping("/")
	public String login(Model model,Authentication auth)
	{
		//Se voglio andare alla homePage, ma sono già loggato come responsabile 
		if(hasRole("RESPONSABILE")) {
			model.addAttribute("log","Benvenuto , "+auth.getName());
			return "/admin/adminPanel";
		}
		else 
		{
			//Se voglio andare alla homePage, ma sono già loggato come direttore
			if(hasRole("DIRETTORE")) {
				model.addAttribute("log",auth.getName());
				return "/superAdmin/superAdminPanel";
			}
		    else 
		    {
		    	//Se voglio andare alla homePage, e non sono loggato
		    	model.addAttribute("centriList",this.centroService.findAll());
		    	return "index";
		    }
		}
	}
	
	protected boolean hasRole(String role) {
        // get security context from thread local
        SecurityContext context = SecurityContextHolder.getContext();
        if (context == null)
            return false;

        Authentication authentication = context.getAuthentication();
        if (authentication == null)
            return false;

        for (GrantedAuthority auth : authentication.getAuthorities()) {
            if (role.equals(auth.getAuthority()))
                return true;
        }
        return false;
    }
	

}
