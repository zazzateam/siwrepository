package siw.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import siw.model.Allievo;
import siw.model.Attivita;
import siw.model.Centro;
import siw.service.AllievoService;
import siw.service.AttivitaService;
import siw.service.ResponsabileService;
import siw.validator.AllievoValidator;

@Controller
public class ControllerUC3 {

	@Autowired
	private AllievoService allievoService;
	@Autowired
	private AllievoValidator allievoValidator;
	@Autowired
	private AttivitaService attivitaService;
	@Autowired
	private ResponsabileService responsabileService;
	
	//CASO D'USO 3 : AGGIUNGERE L'ALLIEVO A UN ATTIVITA
	@RequestMapping("/addAllievo") //Da adminPanel -> EsisteUtenteNelSistema?
	public String addAllievo(Model model) {
		return "admin/inserisciAllievoAttivita";
	}
	
    //3.1 allievo non presente nel sistema          
	@RequestMapping("/nuovoAllievo") //Non esiste -> formRegAllievo
	public String nuovoAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "admin/nuovoAllievo";
	}
	
	@RequestMapping("/scegliAttivita") //formRegAllievo -> scegliAttivita
	public String scegliAttivita(@Valid @ModelAttribute("allievo") Allievo allievo, BindingResult bindingResult, Model model, HttpSession session,Authentication auth)
	{
		if(this.allievoService.findByEmail(allievo.getEmail()) != null)
		{
			model.addAttribute("exist","Allievo già registrato con questa mail");
			return "admin/nuovoAllievo";
		}
		else
		{
			this.allievoValidator.validate(allievo, bindingResult);
			if(!bindingResult.hasErrors()) 
			{
				session.setAttribute("allievo", allievo);
				//Faccio il centro , e mostro solo attività del centro
				Centro c = this.responsabileService.findByEmail(auth.getName()).get(0).getCentro();
				model.addAttribute("attivita", attivitaService.findByCentro_id(c.getId()));
				return "Admin/scegliAttivita";
			}
			else return "admin/nuovoAllievo";
		}
	}
		
	@RequestMapping("/salvaAllievoNuovoAttivita") //scegliAttivita -> salvaDatiNelDb
	public String salvaAllievoNuovoAttivita(@RequestParam("id_attivita") String id, HttpSession session, Model model) 
	{
		Allievo all = (Allievo)session.getAttribute("allievo");
		Attivita att = attivitaService.findById(new Long(id));
		att.getAllievi().add(all);
		all.getAttivita().add(att);
		allievoService.save(all);
		attivitaService.save(att);
		return "admin/inserita";
	}

	//3.2 Allievo già presente nel sistema
	@RequestMapping("/vecchioAllievo") //Esiste -> scegliAllievoEAttivita
	public String vecchioAllievo(Model model,Authentication auth) {
		//Faccio il centro , e mostro solo attività del centro
		Centro c = this.responsabileService.findByEmail(auth.getName()).get(0).getCentro();
		model.addAttribute("attivita", attivitaService.findByCentro_id(c.getId()));
		return "admin/vecchioAllievo";
	}

	@RequestMapping("/salvaAllievoVecchioAttivita")//scegliAllievoEAttivita -> salvaDatiInDb
	public String salvaAllievoVecchioAttivita(@RequestParam("email") String email, @RequestParam("id_attivita") String id, Model model)
	{
		Allievo all = allievoService.findByEmail(email);
		if(all == null)
		{
			model.addAttribute("attivita", attivitaService.findAll());
			model.addAttribute("notExist","Non esiste un utente registrato con ' "+email+"'");
			return "admin/vecchioAllievo";
		}
		else
		{

			Attivita att = attivitaService.findById(new Long(id));
			att.getAllievi().add(all);
			all.getAttivita().add(att);
			allievoService.save(all);
			attivitaService.save(att);
			return "admin/inserita";
		}
	}
}
