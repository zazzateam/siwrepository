package siw.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import siw.model.Attivita;
import siw.model.Centro;
import siw.service.AttivitaService;
import siw.service.CentroService;
import siw.service.ResponsabileService;
import siw.validator.AttivitaValidator;

@Controller
public class ControllerUC2 {
	
	@Autowired
	private AttivitaValidator validator;
	@Autowired
	private AttivitaService attivitaService;
	@Autowired
	private ResponsabileService responsabileService;
	@Autowired
	private CentroService centroService;
	
	//CASO D'USO 2 : INSERIMENTO NUOVA ATTIVITA NEL CENTRO AMMINISTRATO
	@RequestMapping("/addAttività") //Da adminPanel -> formInserimentoAttivita
	public String addAttività(Model model) {
		model.addAttribute("attivita", new Attivita());
		return "admin/inserisciAttivita";
	}
	
	@RequestMapping(value = "/attivita", method = RequestMethod.POST) //Da formInserimentoAttivita -> OK
	public String attivita(@Valid @ModelAttribute("attivita") Attivita attivita, BindingResult bindingResult, Model model,Authentication auth) {  
		
		if(this.attivitaService.findByNomeAndData(attivita.getNome(),attivita.getData()) != null)
		{
			model.addAttribute("exist","Esiste già '"+attivita.getNome()+"' in tale data");
			return "admin/inserisciAttivita";
		}
		this.validator.validate(attivita, bindingResult);
		if(!bindingResult.hasErrors())
		{
			//Ottengo centro
			Centro centro = responsabileService.findByEmail(auth.getName()).get(0).getCentro();
			putIntoDatabase(centro,attivita);
			return "admin/inserita";
		}
		else
			return "admin/inserisciAttivita";
	}
	
	public void putIntoDatabase(Centro c,Attivita a)
	{
		//1 : salvo attivita (poichè il centro già si trova nel db)
		a.setCentro(c);
		attivitaService.save(a);
		//2 : aggiorno centro
		c.getAttivita().add(a);
		centroService.save(c);
	}
}
