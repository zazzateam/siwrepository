package siw.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Allievo {
	
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(nullable=false)
	private String nome;
	@Column(nullable=false)
	private String cognome;
	@Column(nullable=false,unique=true)
	private String email;
	@Column(nullable=false)
	private String luogoNascita;
	@Column(nullable=false) 
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dataNascita;
	@Column(nullable=false)
	private String telefono;
	@ManyToMany(mappedBy="allievi")
	private List<Attivita> attivita;
	
	public Allievo(){}
	
	public Allievo(String n,String c,String e,String l,Date d,String t)
	{
		this.id = null;
		this.nome = n;
		this.cognome = c;
		this.email = e;
		this.luogoNascita = l;
		this.dataNascita = d;
		this.telefono = t;
		this.attivita = new ArrayList<Attivita>();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}
	
	public List<Attivita> getAttivita() {
		if(attivita == null) attivita  = new ArrayList<>();
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}
}
