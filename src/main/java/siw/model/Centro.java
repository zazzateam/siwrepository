package siw.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Centro {
	
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(nullable=false)
	private String nome;
	@Column(nullable=false)
	private String email;
	@Column(nullable=false)
	private String indirizzo;
	@Column(nullable=false)
	private String citta;
	@Column(nullable=false)
	private String telefono;
	@Column(nullable=false)
	private String capienzaMax;
	@OneToOne
	private Responsabile responsabile;
	@OneToMany(mappedBy="centro")
	private List<Attivita> attivita;
	
	public Centro(){}
	
	public Centro(String n,String e,String i,String t,String c,String cit)
	{
		this.id = null;
		this.nome = n;
		this.email = e;
		this.indirizzo = i;
		this.citta = cit;
		this.telefono = t;
		this.capienzaMax = c;
		this.responsabile = null;
		this.attivita = new ArrayList<Attivita>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCapienzaMax() {
		return capienzaMax;
	}

	public void setCapienzaMax(String capienzaMax) {
		this.capienzaMax = capienzaMax;
	}

	public Responsabile getResponsabile() {
		return responsabile;
	}

	public void setResponsabile(Responsabile responsabile) {
		this.responsabile = responsabile;
	}

	public List<Attivita> getAttivita() {
		return attivita;
	}

	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}		
}
