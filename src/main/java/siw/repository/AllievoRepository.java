package siw.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import siw.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long>{
	
	public Allievo findByEmail(String email);

	@Query(nativeQuery = true , value = "select * from allievo join attivita_allievi on allievo.id = attivita_allievi.allievi_id\r" + 
			"                                                  join attivita on attivita.id = attivita_allievi.attivita_id\r" + 
			"                                                  where attivita.id = :idAtt")
	public List<Allievo> prendiAllievi(@Param("idAtt") Long idAtt);
	
	
	@Query(nativeQuery = true , value = "select distinct allievo.* from allievo,attivita_allievi,attivita,centro\r\n" + 
		                             	"where allievo.id = attivita_allievi.allievi_id and \r\n" + 
			                            "attivita.id = attivita_allievi.attivita_id and\r\n" + 
			                            "centro.id = attivita.centro_id and\r\n" + 
			                            "centro.id = :idCentro")
	public List<Allievo> findAllievoDistinct(@Param("idCentro") Long idCentro);
	
	
}
