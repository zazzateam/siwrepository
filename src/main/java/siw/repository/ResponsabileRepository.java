package siw.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import siw.model.Responsabile;

public interface ResponsabileRepository extends CrudRepository<Responsabile, Long>{
	
	public List<Responsabile> findByEmail(String email);

}
