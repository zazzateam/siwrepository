package siw.repository;


import org.springframework.data.repository.CrudRepository;

import siw.model.Centro;

public interface CentroRepository extends CrudRepository<Centro, Long>{
	
	public Centro findByEmail(String email);
	public Centro findByIndirizzoAndCitta(String indirizzo,String citta);
}
