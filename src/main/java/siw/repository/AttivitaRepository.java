package siw.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import siw.model.Attivita;

public interface AttivitaRepository extends CrudRepository<Attivita, Long>{
	
	public List<Attivita> findByData(Date data);
	
    @Query(nativeQuery = true , value = "select * from attivita join attivita_allievi on attivita.id = attivita_allievi.attivita_id join allievo on allievo.id = attivita_allievi.allievi_id where allievo.email = :email")
	public List<Attivita> prendiAttivita(@Param("email") String email);

    public List<Attivita> findByCentro_id(Long centro_id);
    
    public Attivita findByNomeAndData(String nome,Date data);
}
