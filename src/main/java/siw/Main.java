package siw;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import siw.model.Allievo;
import siw.model.Attivita;
import siw.model.Centro;
import siw.model.Responsabile;
import siw.service.AllievoService;
import siw.service.AttivitaService;
import siw.service.CentroService;
import siw.service.ResponsabileService;

@SpringBootApplication
public class Main {
	
	@Autowired
	private ResponsabileService responsabileService;
	@Autowired
	private AttivitaService attivitaService; 
	@Autowired
	private AllievoService allievoService; 
	@Autowired
	private CentroService centroService; 

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}
	
	@Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	@PostConstruct
	public void init() {
		
		BCryptPasswordEncoder b = bCryptPasswordEncoder();
		
		//Salvo centro senza riferimento al responsabile (poichè nel db non esiste)
		Centro c1= new Centro("Centro 1", "centro1@newJob.it", "viale regina margherita 176", "3332253421", "40","roma");
		Centro c2= new Centro("Centro 2", "centro2@newJob.it", "via martiri di via fani 66", "3332234281", "20","sutri");
		Centro c3= new Centro("Centro 3", "centro3@newJob.it", "via giorgio amendola 15", "3765434829", "20","milano");
		Centro c4= new Centro("Centro 4", "centro4@newJob.it", "via del triumvirato 10", "3897654218", "20","bologna");
		Centro c5= new Centro("Centro 5", "centro5@newJob.it", "via roma 25",             "3332746585", "20","torino");
		centroService.save(c1);
		centroService.save(c2);
		centroService.save(c3);
		centroService.save(c4);
		centroService.save(c5);
		//Salvo responsabile con centro assegnato
		Responsabile d = new Responsabile("direttore@newJob.it",b.encode("password"),"DIRETTORE","656565");
		Responsabile r1 = new Responsabile("resp1@newJob.it",b.encode("password1"),"RESPONSABILE","3346757856");
		Responsabile r2 = new Responsabile("resp2@newJob.it",b.encode("password2"),"RESPONSABILE","3767645334");
		Responsabile r3 = new Responsabile("resp3@newJob.it",b.encode("password3"),"RESPONSABILE","3677665734");
		Responsabile r4 = new Responsabile("resp4@newJob.it",b.encode("password4"),"RESPONSABILE","3348765759");
		Responsabile r5 = new Responsabile("resp5@newJob.it",b.encode("password5"),"RESPONSABILE","3340087986");
		responsabileService.save(d);
		r1.setCentro(c1);
		r2.setCentro(c2);
		r3.setCentro(c3);
		r4.setCentro(c4);
		r5.setCentro(c5);
		responsabileService.save(r1);
		responsabileService.save(r2);
		responsabileService.save(r3);
		responsabileService.save(r4);
		responsabileService.save(r5);
		//Setto responsabile (che ora esiste nel db) e aggiorno il centro
		c1.setResponsabile(r1);
		c2.setResponsabile(r2);
		c3.setResponsabile(r3);
		c4.setResponsabile(r4);
		c5.setResponsabile(r5);
		centroService.save(c1);
		centroService.save(c2);
		centroService.save(c3);
		centroService.save(c4);
		centroService.save(c5);
		
		//Salvo allievi senza riferimenti a attività (poichè nel db non esistono)
		Allievo al1 = new Allievo("marco","rossi","MarioRossi@gmail.com","Roma", new Date(), "3935455483");
		Allievo al2 = new Allievo("tommi","zazza","tommizazza@gmail.com","Bologna", new Date(),"3935498768");
		Allievo al3 = new Allievo("Giorgio","zazza","giorgio@gmail.com","Torino", new Date(), "3285564948");
		Allievo al4 = new Allievo("Giuseppe","Verdi","GiusVerdi@gmail.com","Firenze", new Date(), "3983712483");
		Allievo al5 = new Allievo("Antonio","Bruni","AntonioBruni@gmail.com","Roma", new Date(),"3939812368");
		Allievo al6 = new Allievo("Alessio","Scigona","AllScigona@gmail.com","Bologna", new Date(), "3285565438");
		allievoService.save(al1);
		allievoService.save(al2);
		allievoService.save(al3);
		allievoService.save(al4);
		allievoService.save(al5);
		allievoService.save(al6);
		//Salvo Attivita con centro  e allievi assegnati
		Attivita a11 = new Attivita("Utilizzo robot industriali", new Date());
		Attivita a12 = new Attivita("Utilizzo robot industriali", new Date());
		a11.setCentro(c1);
		a11.getAllievi().add(al1);
		a11.getAllievi().add(al2);
		a11.getAllievi().add(al3);
		a12.setCentro(c2);
		a12.getAllievi().add(al4);
		
		Attivita a2 = new Attivita("Introduzione a circuiti", new Date());
		Attivita a22= new Attivita("Introduzione a circuiti", new Date());
		a2.setCentro(c1);
		a2.getAllievi().add(al1);
		a22.setCentro(c5);
		
		Attivita a3 = new Attivita("Corso utilizzo sistemi informatici", new Date());
		Attivita a33= new Attivita("Corso utilizzo sistemi informatici", new Date());
		a3.setCentro(c1);
		a3.getAllievi().add(al3);
		a33.setCentro(c3);
		a33.getAllievi().add(al5);
		a33.getAllievi().add(al6);
		
		attivitaService.save(a11);
		attivitaService.save(a12);
		attivitaService.save(a2);
		attivitaService.save(a3);
		attivitaService.save(a22);
		attivitaService.save(a33);
		
		//Assegno attivita ad allievi e aggiorno
		al1.getAttivita().add(a11);
		al1.getAttivita().add(a2);
		al2.getAttivita().add(a11);
		al3.getAttivita().add(a11);
		al3.getAttivita().add(a3);
		al4.getAttivita().add(a12);
		al5.getAttivita().add(a33);
		al6.getAttivita().add(a33);
		
		allievoService.save(al1);
		allievoService.save(al2);
		allievoService.save(al3);
		allievoService.save(al4);
		allievoService.save(al5);
		allievoService.save(al6);
	}
}
