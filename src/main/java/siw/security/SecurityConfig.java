package siw.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig  extends WebSecurityConfigurerAdapter {
	
    private final String usersQuery = "SELECT email,password,TRUE FROM responsabile WHERE email = ?";
    private final String rolesQuery = "SELECT email,ruolo FROM responsabile WHERE email = ? ";
    
    @Qualifier("dataSource")
    @Autowired
    private javax.sql.DataSource dataSource;
    
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http
        .csrf().disable()
        .authorizeRequests()
        .antMatchers("/", "/index", "/login").permitAll()          
        .antMatchers("/Admin/**").hasAuthority("RESPONSABILE")             
        .antMatchers("/SuperAdmin/**").hasAuthority("DIRETTORE")                     
        .anyRequest().permitAll()
        .anyRequest().authenticated()
        .and()
        .formLogin()
        .loginPage("/login") 					
        .defaultSuccessUrl("/panel")
        .and()
        .logout()
        .logoutSuccessUrl("/login")
        .permitAll()
        .and();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder())
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery);
    }
    
    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
       .antMatchers("/static/**", "/css/**", "/images/**", "/js/**", "/vendor/**");
    }
}