package siw.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import siw.model.Centro;
import siw.repository.CentroRepository;

@Transactional
@Service
public class CentroService {
	@Autowired
	private CentroRepository CentroRepository; 
	
	public Centro save(Centro c) {
		return this.CentroRepository.save(c);
	}

	public List<Centro> findAll() {
		return (List<Centro>) this.CentroRepository.findAll();
	}
	
	public Centro findById(Long id) {
		Optional<Centro> c = this.CentroRepository.findById(id);
		if (c.isPresent()) 
			return c.get();
		else
			return null;
	}	
	
	public Centro findByEmail(String email)
	{
		return this.CentroRepository.findByEmail(email);
	}
	
	public Centro findByIndirizzoAndCitta(String indirizzo,String citta)
	{
		return this.CentroRepository.findByIndirizzoAndCitta(indirizzo,citta);
	}

}
