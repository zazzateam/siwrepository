package siw.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import siw.model.Allievo;
import siw.repository.AllievoRepository;

@Transactional
@Service
public class AllievoService {
	@Autowired
	private AllievoRepository AllievoRepository; 
	
	public Allievo save(Allievo a) {
		return this.AllievoRepository.save(a);
	}

	public List<Allievo> findAll() {
		return (List<Allievo>) this.AllievoRepository.findAll();
	}
	
	public Allievo findById(Long id) {
		Optional<Allievo> a = this.AllievoRepository.findById(id);
		if (a.isPresent()) 
			return a.get();
		else
			return null;
	}	
	
	public Allievo findByEmail(String email)
	{
		Allievo a = this.AllievoRepository.findByEmail(email);
		return a;
	}

	public List<Allievo> prendiAllievi(String idAtt)
	{
		return AllievoRepository.prendiAllievi(new Long(idAtt));
	}
	
	public List<Allievo> AllieviDiUnCentro(Long idCentro)
	{
		return AllievoRepository.findAllievoDistinct(idCentro);
	}
}