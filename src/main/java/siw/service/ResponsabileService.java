package siw.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import siw.model.Responsabile;
import siw.repository.ResponsabileRepository;

@Transactional
@Service
public class ResponsabileService {
	 
	@Autowired
	private ResponsabileRepository responsabileRepository; 
	
	public Responsabile save(Responsabile r) {
		return this.responsabileRepository.save(r);
	}

	public List<Responsabile> findAll() {
		return (List<Responsabile>) this.responsabileRepository.findAll();
	}
	
	public Responsabile findById(Long id) {
		Optional<Responsabile> r = this.responsabileRepository.findById(id);
		if (r.isPresent()) 
			return r.get();
		else
			return null;
	}	
	
	public List<Responsabile> findByEmail(String email)
	{
		return responsabileRepository.findByEmail(email);
	}

}
