package siw.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import siw.model.Attivita;
import siw.repository.AttivitaRepository;

@Transactional
@Service
public class AttivitaService {
	@Autowired
	private AttivitaRepository AttivitaRepository; 
	
	public Attivita save(Attivita a) {
		return this.AttivitaRepository.save(a);
	}

	public List<Attivita> findAll() {
		return (List<Attivita>) this.AttivitaRepository.findAll();
	}
	
	public Attivita findById(Long id) {
		Optional<Attivita> a = this.AttivitaRepository.findById(id);
		if (a.isPresent()) 
			return a.get();
		else
			return null;
	}	
	
	public Attivita findByNomeAndData(String nome,Date data)
	{
		return this.AttivitaRepository.findByNomeAndData(nome,data);
	}
	
	public List<Attivita> findByCentro_id(Long idCentro)
	{
		return AttivitaRepository.findByCentro_id(idCentro);
	}
	
	public List<Attivita> findByDate(Date data)
	{
		return AttivitaRepository.findByData(data);
	}

	public List<Attivita> prendiAttivita(String email)
	{
		return AttivitaRepository.prendiAttivita(email);
	}
	
}